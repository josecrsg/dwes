<?php
    // Simulamos una consulta a la base de datos donde
    // se almacenarán los usuarios en un array $a[]
    $a[] = "J. R. R. Tolkien - El Hobbit";
    $a[] = "J. R. R. Tolkien - La Comunidad del Anillo";
    $a[] = "J. R. R. Tolkien - Las dos torres";
    $a[] = "J. R. R. Tolkien - El retorno del Rey";
    $a[] = "Isaac Asimov - Un guijarro en el cielo";
    $a[] = "Isaac Asimov - Fundación";
    $a[] = "Isaac Asimov - Yo, robot";
    

    // obtenemos el parámetro GET de la URL (Ej: "sugerenciasPHP.php?q=Anna")
    $q = $_REQUEST["q"];

    // Variable que contendrá las coincidencias
    $sugerencias = "";


    //Entra en el bucle si el parámetro obtenido del GET ($q) es diferente a ""
    if ($q !== "") 
    {
        //Si el usuario ha insertado datos se pasan a minúscula
        $q = strtolower($q);
        //Almacenamos la longitud de la palabra
        $len=strlen($q);
        //Ahora vamos a buscar coincidencias en la "base de datos"
        foreach($a as $name) 
        {
            // Buscamos los nombres que contengan la cadena insertada
            if (stristr($q, substr($name, 0, $len))) 
            {
                $sugerencias .= "<br>".$name;
            }
        }
        print $sugerencias;
    }
    // Salida: "no se encuentran sugerencias" si no hay sugerencias
    echo $sugerencias === "" ? "no se encuentran sugerencias" : $sugerencias;
?>

