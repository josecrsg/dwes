<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>Mostrar datos json</title>
 

	</head>

	<body>
		
		<section>
		<?php
        //Llamar api
        $json = file_get_contents("https://datos.comunidad.madrid/catalogo/dataset/bad2b55b-d3d7-4f9e-bbb7-1eb9452ff8e0/resource/9731179a-1e0d-4121-8a7a-40a48531f714/download/registro_sociedades_laborales.json");

		//Decodificar json, extraer datos
		$datos = json_decode($json, true);
      
        foreach($datos["data"] as $dato)
        {
            echo $dato["tipo"];
            echo "; ";
            echo $dato["domicilio"];
            echo "; ";
            echo $dato["denominacion_social"];
            echo "<br> ";
        }

		?>
		</section>		
	</body>
</html>