var validar_usuario = false;
var validar_email = false;
var validar_contrasena = false;
var validar_contrasena_iguales = false;

$("#con2").keyup(function() {
    console.log("keyup");
    if($("#con1").val() != $("#con2").val()){
        validar_contrasena_iguales = false;
        $("#error_con2").addClass("mostrar");
    }
    else{
        validar_contrasena_iguales = true;
        $("#error_con2").removeClass("mostrar");
    }
});
$("#usuario").keyup(function() {
    if($("#usuario").val().length < 3) {
        validar_usuario = false;
        $("#error_usuario").addClass("mostrar");
    }
    else{
        validar_usuario = true;
        $("#error_usuario").removeClass("mostrar");
    }
});
$("#email").keyup(function() {
    var email = $("#email").val().toLowerCase();
    var patron = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if(!patron.test(email)) {
        validar_email = false;
        $("#error_email").addClass("mostrar");
    }
    else{
        validar_email = true;
        $("#error_email").removeClass("mostrar");
    }
});
$("#con1").keyup(function() {
    if($("#con1").val().length < 8) {
        validar_contrasena = false;
        $("#error_con1").addClass("mostrar");
    }
    else{
        validar_contrasena = true;
        $("#error_con1").removeClass("mostrar");
    }
});        
//Capturamos la ejecución del submit
//     si está correcto: se envían los datos al servidor
//     si no está correcto: no se realiza ninguna acción
$("#formulario").submit(function(){
    if(validar_usuario && validar_email && validar_contrasena &&  
        validar_contrasena_iguales){
            console.log("ok");
        return true;
    }
    else{
        console.log("bad");
        return false;
    } 
});

