<?php
//validaciones.php
function validar_usuario($usuario)
{
    if(strlen($usuario) < 3 || !es_texto($usuario)) return false;
    return true;
}

function validar_email($email)
{
    $email = strtolower($email);
    $patron = '/^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9]';
    $patron = $patron.'(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?';
    $patron = $patron.'(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i';
    return (preg_match($patron, $email));
}

function validar_longitud_contrasena($contrasena)
{
    return strlen($contrasena) > 7;
}

function validar_constrasenas_iguales($contrasena1, $contrasena2)
{
    return $contrasena1 == $contrasena2;
}

function es_texto($cadena) 
{
    //definimos el patrón
    $patron = '/^[a-zA-ZñÑ]+$/';     
    return (preg_match($patron, $cadena));
}
?>


