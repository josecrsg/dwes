<!DOCTYPE html>
<html>
    <head>
        <title>Página de registro</title>
        <style>
            .error{
                font-size: 80%;
                color: red;
            }
            .ocultar{
                display: none;
            }
            .mostrar{
                display:inline;
            }
        </style>
    </head>
    <body>
        <form id="formulario" method="POST" action="validaciones.php">
            Usuario:
            <input type="text" name="usuario" id="usuario"/>
            <span class='error ocultar
                <?php 
                    if(isset($_POST['aceptar']) && !validar_usuario($_POST['usuario'])) {
                        echo "mostrar";
                    }
                ?>' id="error_usuario" 
            >Formato incorrecto: requerido mínimo 3 caracteres </span>

            <br><br>
            Email:
            <input type="text" name="email" id="email"/>
            <span class='error ocultar
                <?php 
                    if(isset($_POST['aceptar']) && !validar_email($_POST['email'])){
                        echo "mostrar";
                    } 
                ?>' id="error_email" 
            >Formato incorrecto</span> 

            <br><br>
             Contraseña:
            <input type="password" name="con1" id="con1"/>
            <span class='error ocultar
                <?php 
                    if(isset($_POST['aceptar']) && !validar_longitud_contrasena($_POST['con1'])){
                        echo "mostrar";
                    }
                ?>' id="error_con1" 
            >Formato incorrecto: requerido mínimo 8 caracteres </span> 

            <br><br>
            Repita contraseña:
            <input type="password" name="con2" id="con2"/>
            <span class='error ocultar
                <?php 
                    if(isset($_POST['aceptar']) && !validar_constrasenas_iguales($_POST['con2'])) {
                        echo "mostrar";
                    }
                ?>' id="error_con2" 
            >Las contraseñas deben coincidir </span>

            <br><br>
            <input type="submit" name="aceptar" value="Aceptar">
        </form>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"> </script>
<script src="validaciones.js"></script>

    </body>
</html>

