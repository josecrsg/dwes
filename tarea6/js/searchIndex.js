Search.appendIndex(
    [
                {
            "fqsen": "\\gestionLibros",
            "name": "gestionLibros",
            "summary": "",
            "url": "classes/gestionLibros.html"
        },                {
            "fqsen": "\\gestionLibros\u003A\u003AconsultaDatosLibro\u0028\u0029",
            "name": "consultaDatosLibro",
            "summary": "",
            "url": "classes/gestionLibros.html#method_consultaDatosLibro"
        },                {
            "fqsen": "\\gestionLibros\u003A\u003AconsultarAutores\u0028\u0029",
            "name": "consultarAutores",
            "summary": "",
            "url": "classes/gestionLibros.html#method_consultarAutores"
        },                {
            "fqsen": "\\gestionLibros\u003A\u003AconsultaLibros\u0028\u0029",
            "name": "consultaLibros",
            "summary": "",
            "url": "classes/gestionLibros.html#method_consultaLibros"
        },                {
            "fqsen": "\\gestionLibros\u003A\u003AborrarAutor\u0028\u0029",
            "name": "borrarAutor",
            "summary": "",
            "url": "classes/gestionLibros.html#method_borrarAutor"
        },                {
            "fqsen": "\\gestionLibros\u003A\u003AborrarLibro\u0028\u0029",
            "name": "borrarLibro",
            "summary": "",
            "url": "classes/gestionLibros.html#method_borrarLibro"
        },                {
            "fqsen": "\\",
            "name": "\\",
            "summary": "",
            "url": "namespaces/default.html"
        }            ]
);
